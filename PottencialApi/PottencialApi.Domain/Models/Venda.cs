﻿using PottencialApi.Domain.Enum;
using System;
using System.Collections.Generic;

namespace PottencialApi.Domain.Models
{
    public class Venda
    {
        public Venda()
        { }

        public Venda(int id, DateTime dataPedido, StatusVendaEnum statusVenda, Vendedor vendedor, List<Item> itens)
        {
            Id = id;
            DataPedido = dataPedido;
            StatusVenda = statusVenda;
            Vendedor = vendedor;
            Itens = itens;
        }

        public int Id { get; set; }
        public DateTime DataPedido { get; set; }
        public StatusVendaEnum StatusVenda { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<Item> Itens { get; set; }
    }
}