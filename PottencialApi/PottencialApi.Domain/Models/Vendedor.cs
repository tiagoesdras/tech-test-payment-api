﻿namespace PottencialApi.Domain.Models
{
    public class Vendedor
    {
        public Vendedor()
        { }

        public Vendedor(int id, string cpf, string nome, string email, string telefone)
        {
            Id = id;
            Cpf = cpf;
            Nome = nome;
            Email = email;
            Telefone = telefone;
        }

        public int Id { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}