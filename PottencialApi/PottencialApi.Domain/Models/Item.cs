﻿namespace PottencialApi.Domain.Models
{
    public class Item
    {
        public Item()
        { }

        public Item(int id, string nome, int quantidade, decimal valor)
        {
            Id = id;
            Nome = nome;
            Quantidade = quantidade;
            Valor = valor;
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public int Quantidade { get; set; }
        public decimal Valor { get; set; }
    }
}