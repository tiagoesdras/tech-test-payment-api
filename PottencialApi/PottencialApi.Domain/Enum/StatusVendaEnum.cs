﻿using System.ComponentModel.DataAnnotations;

namespace PottencialApi.Domain.Enum
{
    public enum StatusVendaEnum
    {
        [Display(Name = "Aguardando Pagamento")]
        AguardandoPagamento,

        [Display(Name = "Pagamento Aprovado")]
        PagamentoAprovado,

        [Display(Name = "Enviado para Transportadora")]
        EnviadoTransportadora,

        [Display(Name = "Entregue")]
        Entregue,

        [Display(Name = "Cancelada")]
        Cancelada,
    }
}