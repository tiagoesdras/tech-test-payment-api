﻿using ExpectedObjects;
using PottencialApi.Domain.Models;
using Xunit;

namespace PottencialApi.Domain.Test.Itens
{
    public class ItemTest
    {
        private readonly DadosFake.DadosFake _dadosFake;

        public ItemTest()
        {
            _dadosFake = new DadosFake.DadosFake();
        }

        //Criar item com id, nome, quantidade, e valor;
        [Fact(DisplayName = "DeveCriarItem")]
        public void DeveCriarItem()
        {
            var item = new Item(_dadosFake.Item1.Id, _dadosFake.Item1.Nome, _dadosFake.Item1.Quantidade, _dadosFake.Item1.Valor);

            item.ToExpectedObject().ShouldMatch(_dadosFake.Item1);
        }
    }
}