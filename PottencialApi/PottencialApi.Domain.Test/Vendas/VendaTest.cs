﻿using ExpectedObjects;
using PottencialApi.Domain.Models;
using Xunit;

namespace PottencialApi.Domain.Test.Vendas
{
    public class VendaTest
    {
        private readonly DadosFake.DadosFake _dadosFake;

        public VendaTest()
        {
            _dadosFake = new DadosFake.DadosFake();
        }

        //Criar venda com id, data, status, vendedor e lista de itens;
        [Fact(DisplayName = "DeveCriarUmaVenda")]
        public void DeveCriarUmaVenda()
        {
            var venda = new Venda(
                _dadosFake.Venda1.Id,
                _dadosFake.Venda1.DataPedido,
                _dadosFake.Venda1.StatusVenda,
                _dadosFake.Venda1.Vendedor,
                _dadosFake.Venda1.Itens
              );

            venda.ToExpectedObject().ShouldMatch(_dadosFake.Venda1);
        }
    }
}