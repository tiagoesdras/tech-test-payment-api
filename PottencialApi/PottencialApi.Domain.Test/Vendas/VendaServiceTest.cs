﻿using AutoMapper;
using Moq;
using PottencialApi.Application;
using PottencialApi.Application.Dtos;
using PottencialApi.Domain.Enum;
using PottencialApi.Domain.Models;
using PottencialApi.Repositorio.Contratos;
using System;
using System.Collections.Generic;
using Xunit;

namespace PottencialApi.Domain.Test.Vendas
{
    public class VendaServiceTest
    {
        private readonly VendaService _vendaService;
        private readonly Mock<IVendaRepositorio> _vendaRepositorioMock;
        private readonly Mock<IMapper> _mapperMock;

        private readonly DadosFake.DadosFake _dadosFake;

        private readonly List<ItemDto> _itensDto;
        private readonly ItemDto _itemDto;
        private readonly VendedorDto _vendedorDto;
        private readonly VendaAddDto _vendaAddDto;
        private readonly VendaDto _vendaDto;
        private readonly VendaDto _vendaDto2;
        private readonly VendaDto _vendaDto3;

        public VendaServiceTest()
        {
            _vendaRepositorioMock = new Mock<IVendaRepositorio>();
            _mapperMock = new Mock<IMapper>();
            _vendaService = new VendaService(_vendaRepositorioMock.Object, _mapperMock.Object);

            _dadosFake = new DadosFake.DadosFake();

            _itensDto = new List<ItemDto>();

            foreach (var item in _dadosFake.Venda1.Itens)
            {
                _itemDto = new ItemDto()
                {
                    Id = item.Id,
                    Nome = item.Nome,
                    Quantidade = item.Quantidade,
                    Valor = item.Valor
                };

                _itensDto.Add(_itemDto);
            }

            _vendedorDto = new VendedorDto
            {
                Id = _dadosFake.Vendedor1.Id,
                Nome = _dadosFake.Vendedor1.Nome,
                Cpf = _dadosFake.Vendedor1.Cpf,
                Email = _dadosFake.Vendedor1.Email,
                Telefone = _dadosFake.Vendedor1.Telefone
            };

            _vendaAddDto = new VendaAddDto
            {
                Id = _dadosFake.Venda1.Id,
                Itens = _itensDto,
                Vendedor = _vendedorDto
            };

            _vendaDto = new VendaDto
            {
                Id = _dadosFake.Venda1.Id,
                Itens = _itensDto,
                Vendedor = _vendedorDto,
                StatusVenda = StatusVendaEnum.AguardandoPagamento
            };

            _vendaDto2 = new VendaDto
            {
                Id = _dadosFake.Venda2.Id,
                Itens = _itensDto,
                Vendedor = _vendedorDto,
                StatusVenda = StatusVendaEnum.PagamentoAprovado
            };

            _vendaDto3 = new VendaDto
            {
                Id = _dadosFake.Venda3.Id,
                Itens = _itensDto,
                Vendedor = _vendedorDto,
                StatusVenda = StatusVendaEnum.EnviadoTransportadora
            };
        }

        [Fact(DisplayName = "DeveAdicionarVenda")]
        public void DeveAdicionarVenda()
        {
            _vendaService.AdicionarVenda(_vendaAddDto);
            _vendaRepositorioMock.Verify(v => v.Adicionar(It.IsAny<Venda>()));
        }

        [Fact(DisplayName = "ObterVendaPorIdValido")]
        public void ObterVendaPorIdValido()
        {
            _vendaRepositorioMock.Setup(v => v.ObterPorId(It.IsAny<int>())).Returns(_dadosFake.Venda1);
            _mapperMock.Setup(v => v.Map<VendaDto>(_dadosFake.Venda1)).Returns(_vendaDto);
            var venda = _vendaService.ObterVendaPorId(_dadosFake.Venda1.Id);
            Assert.False(venda == null);
        }

        [Fact(DisplayName = "NadoDeveObterVendaComIdInexistente")]
        public void NadoDeveObterVendaComIdInexistente()
        {
            _vendaRepositorioMock.Setup(v => v.ObterPorId(_dadosFake.IdInvalido)).Returns(_dadosFake.Venda1);
            _mapperMock.Setup(v => v.Map<VendaDto>(_dadosFake.Venda1)).Returns(_vendaDto);

            Assert.Throws<ArgumentException>(() => _vendaService.ObterVendaPorId(_dadosFake.Venda2.Id)).ComMensagem("Id de venda inexistente");
        }

        [Fact(DisplayName = "NaoDeveAtualizarStatusDaVendaTesteDeRegraAguardandoPagamento")]
        public void NaoDeveAtualizarStatusDaVendaTesteDeRegraAguardandoPagamento()
        {
            _vendaRepositorioMock.Setup(v => v.ObterPorId(It.IsAny<int>())).Returns(_dadosFake.Venda1);
            _mapperMock.Setup(v => v.Map<VendaDto>(_dadosFake.Venda1)).Returns(_vendaDto);
            var vendaDto = _vendaService.ObterVendaPorId(_dadosFake.Venda1.Id);

            Assert.Throws<ArgumentException>(() => _vendaService.AtualizarVenda(vendaDto.Id, StatusVendaEnum.EnviadoTransportadora)).ComMensagem($"Não é possível atualizar o status da venda de {vendaDto.StatusVenda} para {StatusVendaEnum.EnviadoTransportadora}");
        }

        [Fact(DisplayName = "DeveAtualizarStatusDaVendaTesteDeRegraAguardandoPagamento")]
        public void DeveAtualizarStatusDaVendaTesteDeRegraAguardandoPagamento()
        {
            _vendaRepositorioMock.Setup(v => v.ObterPorId(It.IsAny<int>())).Returns(_dadosFake.Venda1);
            _mapperMock.Setup(v => v.Map<VendaDto>(_dadosFake.Venda1)).Returns(_vendaDto);
            var vendaDto = _vendaService.ObterVendaPorId(_dadosFake.Venda1.Id);

            _vendaService.AtualizarVenda(vendaDto.Id, StatusVendaEnum.PagamentoAprovado);
            _vendaService.AtualizarVenda(vendaDto.Id, StatusVendaEnum.Cancelada);
            _vendaRepositorioMock.Verify(v => v.Atualizar(It.IsAny<Venda>()), Times.Exactly(2));
        }

        [Fact(DisplayName = "NaoDeveAtualizarStatusDaVendaTesteDeRegraPagamentoAprovado")]
        public void NaoDeveAtualizarStatusDaVendaTesteDeRegraPagamentoAprovado()
        {
            _vendaRepositorioMock.Setup(v => v.ObterPorId(It.IsAny<int>())).Returns(_dadosFake.Venda2);
            _mapperMock.Setup(v => v.Map<VendaDto>(_dadosFake.Venda2)).Returns(_vendaDto2);

            var vendaDto = _vendaService.ObterVendaPorId(_dadosFake.Venda2.Id);
            Assert.Throws<ArgumentException>(() => _vendaService.AtualizarVenda(vendaDto.Id, StatusVendaEnum.Entregue)).ComMensagem($"Não é possível atualizar o status da venda de {vendaDto.StatusVenda} para {StatusVendaEnum.Entregue}");
        }

        [Fact(DisplayName = "DeveAtualizarStatusDaVendaTesteDeRegraPagamentoAprovado")]
        public void DeveAtualizarStatusDaVendaTesteDeRegraPagamentoAprovado()
        {
            _vendaRepositorioMock.Setup(v => v.ObterPorId(It.IsAny<int>())).Returns(_dadosFake.Venda2);
            _mapperMock.Setup(v => v.Map<VendaDto>(_dadosFake.Venda2)).Returns(_vendaDto2);
            var vendaDto = _vendaService.ObterVendaPorId(_dadosFake.Venda2.Id);

            _vendaService.AtualizarVenda(vendaDto.Id, StatusVendaEnum.EnviadoTransportadora);            
            _vendaRepositorioMock.Verify(v => v.Atualizar(It.IsAny<Venda>()), Times.Exactly(1));
        }

        [Fact(DisplayName = "NaoDeveAtualizarStatusDaVendaTesteDeRegraEnviadoParaTransportadora")]
        public void NaoDeveAtualizarStatusDaVendaTesteDeRegraEnviadoParaTransportadora()
        {
            _vendaRepositorioMock.Setup(v => v.ObterPorId(It.IsAny<int>())).Returns(_dadosFake.Venda3);
            _mapperMock.Setup(v => v.Map<VendaDto>(_dadosFake.Venda3)).Returns(_vendaDto3);
            var vendaDto = _vendaService.ObterVendaPorId(_dadosFake.Venda3.Id);

            Assert.Throws<ArgumentException>(() => _vendaService.AtualizarVenda(_vendaDto3.Id, StatusVendaEnum.Cancelada)).ComMensagem($"Não é possível atualizar o status da venda de {vendaDto.StatusVenda} para {StatusVendaEnum.Cancelada}");
        }

        [Fact(DisplayName = "DeveAtualizarStatusDaVendaTesteDeRegraEnviadoParaTransportadora")]
        public void DeveAtualizarStatusDaVendaTesteDeRegraEnviadoParaTransportadora()
        {
            _vendaRepositorioMock.Setup(v => v.ObterPorId(It.IsAny<int>())).Returns(_dadosFake.Venda3);
            _mapperMock.Setup(v => v.Map<VendaDto>(_dadosFake.Venda3)).Returns(_vendaDto3);            
            var vendaDto = _vendaService.ObterVendaPorId(_dadosFake.Venda3.Id);

            _vendaService.AtualizarVenda(vendaDto.Id, StatusVendaEnum.Entregue);
            _vendaRepositorioMock.Verify(v => v.Atualizar(It.IsAny<Venda>()), Times.Exactly(1));
        }
    }
}