﻿using PottencialApi.Domain.Enum;
using PottencialApi.Domain.Models;
using System;
using System.Collections.Generic;

namespace PottencialApi.Domain.Test.DadosFake
{
    public class DadosFake
    {
        public int IdInvalido { get; set; }
        public Item Item1 { get; set; }
        public Item Item2 { get; set; }
        public Item Item3 { get; set; }
        public Vendedor Vendedor1 { get; set; }
        public Vendedor Vendedor2 { get; set; }
        public Venda Venda1 { get; set; }
        public Venda Venda2 { get; set; }
        public Venda Venda3 { get; set; }

        public DadosFake()
        {
            IdInvalido = 95;

            Item1 = new Item(1, "Produto 1", 10, 29.99m);
            Item2 = new Item(2, "Produto 2", 19, 89.99m);
            Item3 = new Item(3, "Produto 3", 37, 629.99m);

            Vendedor1 = new Vendedor(1, "58770902593", "Nome 1", "nome1@teste.com", "99999999999");
            Vendedor2 = new Vendedor(2, "17682152788", "Nome 2", "nome2@teste.com", "88888888888");

            Venda1 = new Venda(1, DateTime.Now, StatusVendaEnum.AguardandoPagamento, Vendedor1, new List<Item>
            {
                Item1,
                Item2,
                Item3
            });

            Venda2 = new Venda(2, DateTime.Now, StatusVendaEnum.PagamentoAprovado, Vendedor2, new List<Item>
            {
                Item1,
                Item2,
                Item3
            });

            Venda3 = new Venda(3, DateTime.Now, StatusVendaEnum.EnviadoTransportadora, Vendedor1, new List<Item>
            {
                Item1,
                Item2,
                Item3
            });
        }
    }
}