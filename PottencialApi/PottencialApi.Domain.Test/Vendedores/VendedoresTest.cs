﻿using ExpectedObjects;
using PottencialApi.Domain.Models;
using Xunit;

namespace PottencialApi.Domain.Test.Vendedores
{
    public class VendedorTest
    {
        private readonly DadosFake.DadosFake _dadosFake;

        public VendedorTest()
        {
            _dadosFake = new DadosFake.DadosFake();
        }

        //Criar vendedor com id, cpf, nome, e-mail e telefone;
        [Fact(DisplayName = "DeveCriarVendedor")]
        public void DeveCriarVendedor()
        {
            var vendedor = new Vendedor(_dadosFake.Vendedor1.Id,
                _dadosFake.Vendedor1.Cpf,
                _dadosFake.Vendedor1.Nome,
                _dadosFake.Vendedor1.Email,
                _dadosFake.Vendedor1.Telefone);

            vendedor.ToExpectedObject().ShouldMatch(_dadosFake.Vendedor1);
        }
    }
}