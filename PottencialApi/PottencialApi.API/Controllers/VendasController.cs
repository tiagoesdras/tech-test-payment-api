﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PottencialApi.Application.Contratos;
using PottencialApi.Application.Dtos;
using PottencialApi.Domain.Enum;
using System;

namespace PottencialApi.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly IVendaService _vendaService;

        public VendasController(IVendaService vendaService)
        {
            _vendaService = vendaService;
        }

        [HttpPost]
        public IActionResult Post(VendaAddDto vendaDto)
        {
            try
            {
                var venda = _vendaService.AdicionarVenda(vendaDto);

                if (venda == null) return BadRequest();

                return Ok(venda);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                $"Erro ao tentar adicionar venda. Erro: {ex.Message}");
            }
        }

        [HttpGet("VendaId")]
        public IActionResult GetById(int vendaId)
        {
            try
            {
                var venda = _vendaService.ObterVendaPorId(vendaId);

                if (venda == null) return NotFound();

                return Ok(venda);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                $"Erro ao tentar recuperar venda de Id: {vendaId}. Erro: {ex.Message}");
            }
        }

        [HttpPut("AtualizarStatusVendaPorVendaId")]
        public IActionResult Put(int vendaId, StatusVendaEnum statusVenda)
        {
            try
            {
                var venda = _vendaService.AtualizarVenda(vendaId, statusVenda);

                if (venda == null) return BadRequest();

                return Ok(venda);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                $"Erro: {ex.Message}");
            }
        }
    }
}