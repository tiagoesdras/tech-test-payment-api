﻿using AutoMapper;
using PottencialApi.Application.Dtos;
using PottencialApi.Domain.Models;

namespace PottencialApi.API.Helpers
{
    public class PottencialApiProfile : Profile
    {
        public PottencialApiProfile()
        {
            CreateMap<Venda, VendaDto>().ReverseMap();
            CreateMap<Venda, VendaAddDto>().ReverseMap();
            CreateMap<Item, ItemDto>().ReverseMap();
            CreateMap<Vendedor, Vendedor>().ReverseMap();
        }
    }
}