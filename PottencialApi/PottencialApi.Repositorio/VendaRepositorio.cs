﻿using Microsoft.EntityFrameworkCore;
using PottencialApi.Domain.Models;
using PottencialApi.Repositorio.Contratos;
using System.Linq;

namespace PottencialApi.Repositorio
{
    public class VendaRepositorio : IVendaRepositorio
    {
        private readonly DatabaseContext _databaseContext;

        public VendaRepositorio(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public Venda Adicionar(Venda venda)
        {
            _databaseContext.AddAsync(venda);
            _databaseContext.SaveChangesAsync();
            return venda;
        }

        public Venda Atualizar(Venda venda)
        {
            var vendaASerAtualizada = _databaseContext.Vendas
                  .Where(v => v.Id == venda.Id)
                 .FirstOrDefault();

            vendaASerAtualizada.StatusVenda = venda.StatusVenda;
            _databaseContext.Vendas.Update(vendaASerAtualizada);

            _databaseContext.SaveChanges();
            return venda;
        }

        public Venda ObterPorId(int id)
        {
            return _databaseContext.Vendas
                .Include(v => v.Vendedor)
                .Include(v => v.Itens)
                .Where(v => v.Id == id)
                .FirstOrDefault();
        }
    }
}