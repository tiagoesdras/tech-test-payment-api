﻿using Microsoft.EntityFrameworkCore;
using PottencialApi.Domain.Models;

namespace PottencialApi.Repositorio
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        public DbSet<Item> Itens { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Venda> Vendas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Venda>()
                .HasMany(v => v.Itens);

            modelBuilder.Entity<Venda>()
                .HasOne(v => v.Vendedor);
        }
    }
}