﻿using PottencialApi.Domain.Models;

namespace PottencialApi.Repositorio.Contratos
{
    public interface IVendaRepositorio
    {
        Venda ObterPorId(int id);

        Venda Adicionar(Venda venda);

        Venda Atualizar(Venda venda);
    }
}