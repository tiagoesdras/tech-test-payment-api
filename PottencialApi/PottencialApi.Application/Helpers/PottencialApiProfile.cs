﻿using AutoMapper;
using PottencialApi.Application.Dtos;
using PottencialApi.Domain.Models;

namespace PottencialApi.Application.Helpers
{
    public class PottencialApiProfile : Profile
    {
        public PottencialApiProfile()
        {
            CreateMap<Venda, VendaAddDto>().ReverseMap();
            CreateMap<Venda, VendaDto>().ReverseMap();
            CreateMap<Item, ItemDto>().ReverseMap();
            CreateMap<Vendedor, VendedorDto>().ReverseMap();
        }
    }
}