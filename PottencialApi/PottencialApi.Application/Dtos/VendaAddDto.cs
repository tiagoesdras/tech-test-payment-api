﻿using PottencialApi.Domain.Enum;
using System;
using System.Collections.Generic;

namespace PottencialApi.Application.Dtos
{
    public class VendaAddDto
    {
        public int Id { get; set; }
        public DateTime DataPedido { get; private set; } = DateTime.Now;
        public StatusVendaEnum StatusVenda { get; private set; }
        public VendedorDto Vendedor { get; set; }
        public List<ItemDto> Itens { get; set; }
    }
}