﻿namespace PottencialApi.Application.Dtos
{
    public class ItemDto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int Quantidade { get; set; }
        public decimal Valor { get; set; }
    }
}