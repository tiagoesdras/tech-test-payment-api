﻿using PottencialApi.Domain.Enum;
using System;
using System.Collections.Generic;

namespace PottencialApi.Application.Dtos
{
    public class VendaDto
    {
        public int Id { get; set; }
        public DateTime DataPedido { get; set; }
        public StatusVendaEnum StatusVenda { get; set; }
        public VendedorDto Vendedor { get; set; }
        public List<ItemDto> Itens { get; set; }
    }
}