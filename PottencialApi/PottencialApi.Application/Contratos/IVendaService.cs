﻿using PottencialApi.Application.Dtos;
using PottencialApi.Domain.Enum;

namespace PottencialApi.Application.Contratos
{
    public interface IVendaService
    {
        VendaDto AdicionarVenda(VendaAddDto vendaDto);

        VendaDto ObterVendaPorId(int id);

        VendaDto AtualizarVenda(int id, StatusVendaEnum statusVendaASerAtualizado);
    }
}