﻿using AutoMapper;
using PottencialApi.Application.Contratos;
using PottencialApi.Application.Dtos;
using PottencialApi.Domain.Enum;
using PottencialApi.Domain.Models;
using PottencialApi.Repositorio.Contratos;
using System;

namespace PottencialApi.Application
{
    public class VendaService : IVendaService
    {
        private readonly IVendaRepositorio _vendaRepositorio;
        private readonly IMapper _mapper;

        public VendaService(IVendaRepositorio vendaRepositorio, IMapper mapper)
        {
            _vendaRepositorio = vendaRepositorio;
            _mapper = mapper;
        }

        public VendaService(IVendaRepositorio vendaRepositorio)
        {
            _vendaRepositorio = vendaRepositorio;
        }

        public VendaDto AdicionarVenda(VendaAddDto vendaDto)
        {
            var venda = _mapper.Map<Venda>(vendaDto);

            var vendaRetorno = _vendaRepositorio.Adicionar(venda);

            return _mapper.Map<VendaDto>(vendaRetorno);
        }

        public VendaDto AtualizarVenda(int id, StatusVendaEnum statusVendaASerAtualizado)
        {
            var vendaDto = ObterVendaPorId(id);

            VerificarSeAtualizacaoDeStatusEhValida(vendaDto.StatusVenda, statusVendaASerAtualizado);

            vendaDto.StatusVenda = statusVendaASerAtualizado;

            var venda = _mapper.Map<Venda>(vendaDto);

            var vendaRetorno = _vendaRepositorio.Atualizar(venda);

            return _mapper.Map<VendaDto>(vendaRetorno);
        }

        public VendaDto ObterVendaPorId(int id)
        {
            var venda = _vendaRepositorio.ObterPorId(id);

            if (venda == null)
                throw new ArgumentException("Id de venda inexistente");

            return _mapper.Map<VendaDto>(venda);
        }

        private static void VerificarSeAtualizacaoDeStatusEhValida(StatusVendaEnum statusVenda, StatusVendaEnum statusASerAtualizado)
        {
            var lancarException = false;

            if (statusVenda == StatusVendaEnum.AguardandoPagamento &&
                (statusASerAtualizado != StatusVendaEnum.PagamentoAprovado &&
                statusASerAtualizado != StatusVendaEnum.Cancelada))
            {
                lancarException = true;
            }
            else if (statusVenda == StatusVendaEnum.PagamentoAprovado &&
                (statusASerAtualizado != StatusVendaEnum.EnviadoTransportadora &&
                statusASerAtualizado != StatusVendaEnum.Cancelada))
            {
                lancarException = true;
            }
            else if (statusVenda == StatusVendaEnum.EnviadoTransportadora &&
                statusASerAtualizado != StatusVendaEnum.Entregue)
            {
                lancarException = true;
            }
            if (lancarException)
                throw new ArgumentException($"Não é possível atualizar o status da venda de {statusVenda} para {statusASerAtualizado}");
        }
    }
}